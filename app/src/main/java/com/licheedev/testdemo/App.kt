package com.licheedev.testdemo

import android.app.Application
import android.content.Intent
import com.licheedev.myutils.LogPlus
import com.licheedev.silentinstall.ApkInfo
import com.licheedev.silentinstall.SilentInstall


class App : Application() {

    override fun onCreate() {
        super.onCreate()

        SilentInstall.setup {
            //LogPlus.e(ApkInfo.thisVersionName)
            val intent = SilentInstall.launchIntent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

}