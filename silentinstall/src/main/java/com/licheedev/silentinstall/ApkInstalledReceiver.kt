package com.licheedev.silentinstall

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class ApkInstalledReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {


        if (intent.action == Intent.ACTION_MY_PACKAGE_REPLACED) {
            Log.i(
                "ApkInstalledReceiver",
                "ApkInfo,versionName=${ApkInfo.thisVersionName},versionCode=${ApkInfo.thisVersionCode}"
            )
            SilentInstall.packageReplacedAction?.invoke(intent)
        }
    }
}