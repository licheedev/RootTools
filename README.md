# RootTools
默认要求设备已经获取root权限, root操作依赖了[RootManager](https://github.com/Chrisplus/RootManager)

## 根build.gradle添加
```gradle
allprojects {
  repositories {
    ...
    mavenCentral()
    jcenter()
  }
}
```

## 亮屏、息屏
```gradle
// 亮屏、息屏
implementation 'com.licheedev:root-screenpower:1.0.2'
```
使用
```kotlin
GlobalScope.launch(Dispatchers.IO) {
    val screenPower = RootScreenPower(this@MainActivity)
    // 具体操作需要在子线程中执行，这里用的协程
    screenPower.screenOff() // 息屏
    delay(5000)
    screenPower.screenOn() // 亮屏
}
```

## 静默安装
```gradle
// 静默安装
implementation 'com.licheedev:root-silentinstall:1.0.2'
```
使用
```kotlin
// 在Application中配置回调，用来自动启动应用
SilentInstall.setup {
    //LogPlus.e(ApkInfo.thisVersionName)
    val intent = SilentInstall.launchIntent(this, MainActivity::class.java)
    startActivity(intent)
}

// 安装apk
GlobalScope.launch(Dispatchers.IO) {
    val dir = application.getExternalFilesDir("apks")
    dir?.mkdirs()
    val file = File(dir, "ver999_9.9.9_debug.apk")
    Log.i("ApkInfo", ApkInfo(file).toString()) // 打印apk文件信息
    
    SilentInstall.install(file) // 执行安装，需要在子线程中执行
}
```